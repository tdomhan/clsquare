[Main]
num_episodes = 500
cycles_per_episode = 60

plant = KheperaMazePlant
#controller = QTableController
#controller = DemoController
#controller = TreeController
controller = GaussianProcessController
graphic = KheperaMazeGraphic
statistics = GeneralStatistics
reward = RewardFromMeasurement

sleep_every_cycle = 100
#sleep_every_cycle = 0

verbosity = 10

[Input]
input_mode = from_file
input_file = Maze.init

#input_mode = random
#xinit = [ 10] [0 10] 


[Output]

#[Statistics]
#statistics_mode = standardized
#xwork = [0 11] [0 11] 
#xplus = [7.5 8.5] [1.5 2.5]
#average_over_n_episodes = 10
#statistics_file = train.stat

[Reward]
#The first measurement dimension will be used as a reward indicator
which_representation_dimension = 0
reward_step = 0
reward_xplus = 100

[Controller]
actions = 0 1 2 3
fileprefix = controller
update_freq = 0
epsilon = 0.0
training = false
 
[Graphic]
active = false
#port = 20000 #port for tcp
