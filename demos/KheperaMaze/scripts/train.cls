[Main]
num_episodes = 500
cycles_per_episode = 60

plant = KheperaMazePlant
#controller = QTableController
#controller = DemoController
#controller = TreeController
controller = GaussianProcessController
graphic = KheperaMazeGraphic
statistics = GeneralStatistics
reward = RewardFromMeasurement

#optional parameters:
call_cmd_freq = 100
call_cmd = ./test

#sleep_every_cycle = 100
#sleep_every_cycle = 500
sleep_every_cycle = 0

verbosity = 10

[Input]
input_mode = from_file
input_file = Maze.init

#input_mode = random
#xinit = [ 10] [0 10] 


[Output]

#[Statistics]
#statistics_mode = standardized
#xwork = [0 11] [0 11] 
#xplus = [7.5 8.5] [1.5 2.5]
#average_over_n_episodes = 10
#statistics_file = train.stat

[Reward]
#The first measurement dimension will be used as a reward indicator
which_representation_dimension = 0
reward_step = 0
reward_xplus = 100
xplus_is_terminal = true

[Controller]
actions = 0 1 2 3
fileprefix = controller
training = true
save_frequency = 1
update_freq = 10
epsilon = 0.5
gamma = 0.95

#GP things
fq_cycles = 10
covfunc = CovSum(CovMatern5iso, CovNoise)
hyperparam = 0.7 0.9 0.05
 
[Graphic]
active = false
#port = 20000 #port for tcp
