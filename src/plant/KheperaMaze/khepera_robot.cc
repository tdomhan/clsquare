#include <khepera_robot.h>



double RayCaster::cast() {
  
  //calculate points of ray
  float angle = m_source_body->GetAngle() + m_ray_angle_offset;
  b2Vec2 p2 = m_source_body->GetPosition() + m_ray_length * b2Vec2( cosf(angle), sinf(angle) );
  
  //set up input
  b2RayCastInput input;
  input.p1 = m_source_body->GetPosition();
  input.p2 = p2;
  input.maxFraction = 1;
  
  //check every fixture of every body to find closest
  float closestFraction = 1; //start with end of line as p2
  b2Vec2 intersectionNormal(0,0);
  for (b2Body* b = m_world->GetBodyList(); b; b = b->GetNext()) {
    for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
      
      b2RayCastOutput output;
      if ( ! f->RayCast( &output, input, 0 ) )
      continue;
      if ( output.fraction < closestFraction ) {
        closestFraction = output.fraction;
        intersectionNormal = output.normal;
      }
    }
  }
  
  b2Vec2 intersectionPoint = m_source_body->GetPosition() + closestFraction * (p2 - m_source_body->GetPosition());
  
  //std::cout << m_ray_length * closestFraction << std::endl;
  
  //drawOpenGL(intersectionPoint);
  
  return m_ray_length * closestFraction;
}


KheperaRobot::KheperaRobot(b2World* world) :
m_world(world),
m_move_state(MS_STOP),
m_type(ROBOT)
{
  b2BodyDef myBodyDef;
  myBodyDef.type = b2_dynamicBody; //this will be a dynamic body
  myBodyDef.position.Set(1, 1); //set the starting position
  myBodyDef.angle = 0; //set the starting angle
  m_body = m_world->CreateBody(&myBodyDef);
  
  b2CircleShape circleShape;
  circleShape.m_radius = ZOOM * 0.13 * 0.5; //width of Khepera: 130mm
  
  b2FixtureDef boxFixtureDef;
  boxFixtureDef.shape = &circleShape;
  boxFixtureDef.density = 0.690;//690g
  m_body->CreateFixture(&boxFixtureDef);
  
  rays.push_back(new RayCaster(world, m_body,     0));
  rays.push_back(new RayCaster(world, m_body,   40 * DEGTORAD));
  rays.push_back(new RayCaster(world, m_body,  -40 * DEGTORAD));
  rays.push_back(new RayCaster(world, m_body,   80 * DEGTORAD));
  rays.push_back(new RayCaster(world, m_body,  -80 * DEGTORAD));
  rays.push_back(new RayCaster(world, m_body,  140 * DEGTORAD));
  rays.push_back(new RayCaster(world, m_body, -140 * DEGTORAD));
  rays.push_back(new RayCaster(world, m_body,  180 * DEGTORAD));
  
  m_body->SetUserData(&m_type);
}

void KheperaRobot::step() {
    
  b2Vec2 vel = m_body->GetLinearVelocity();
  double SPEED = ZOOM * 0.3;//0.5 m/s
  switch ( m_move_state )
  {
      //case MS_LEFT:  vel.x = -SPEED*cos(m_body->GetAngle()); vel.y = -SPEED*sin(m_body->GetAngle()); break;
    case MS_STOP:
      vel.x =  0;
      vel.y = 0;
      m_body->SetAngularVelocity(0);
      break;
    case MS_FORWARD:
      vel.x =  SPEED*cos(m_body->GetAngle());
      vel.y =  SPEED*sin(m_body->GetAngle());
      m_body->SetAngularVelocity(0);
      break;
    case MS_TURN_LEFT:
      vel.x =  SPEED*cos(m_body->GetAngle());
      vel.y =  SPEED*sin(m_body->GetAngle());
      m_body->SetAngularVelocity(3);
      break;
    case MS_TURN_RIGHT:
      vel.x =  SPEED*cos(m_body->GetAngle());
      vel.y =  SPEED*sin(m_body->GetAngle());
      m_body->SetAngularVelocity(-3);
      break;
  }
  m_body->SetLinearVelocity(vel);
  
  for(std::vector<RayCaster*>::iterator it = rays.begin(); it != rays.end(); ++it) {
    RayCaster* ray = *it;
    ray->cast();
  }
}

void KheperaRobot::set_move_state(MoveState move_state) {
  m_move_state = move_state;
}

void KheperaRobot::set_state(double x, double y, double angle) {
  m_body->SetTransform(b2Vec2(x, y), angle);
}


double KheperaRobot::get_x() {
  b2Vec2 pos = m_body->GetPosition();
  return pos.x;
}

double KheperaRobot::get_y() {
  b2Vec2 pos = m_body->GetPosition();
  return pos.y;
}

double KheperaRobot::get_theta() {
  return m_body->GetAngle();
}


KheperaRobot::~KheperaRobot(){
  m_world->DestroyBody(m_body);
  
  for(std::vector<RayCaster*>::iterator it = rays.begin(); it != rays.end(); ++it) {
    RayCaster* ray = *it;
    delete ray;
  }
}


