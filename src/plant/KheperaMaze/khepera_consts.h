#ifndef _KHEPERA_CONSTS_H_
#define _KHEPERA_CONSTS_H_

enum WORLD_OBJECT_TYPE {MAZE_BOX, ROBOT};

//Scale all sizes by the following factor
//(so that they won't be tiny in the Box2D TestBed)
#define ZOOM 10

#endif