/*
clsquare - closed loop simulation system
Copyright (c) 2004, Neuroinformatics Group, Prof. Dr. Martin Riedmiller,
University of Osnabrueck
Copyright (c) 2011, Machine Learning Lab, Prof. Dr. Martin Riedmiller,
University of Freiburg

khepera_maze.cc: a 2D maze for the Khepera robot.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the <ORGANIZATION> nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
*/



#ifndef _KHEPERA_MAZE_PLANT_H_
#define _KHEPERA_MAZE_PLANT_H_

#include "plant.h"
#include <khepera_world.h>

#define STATE_DIM 4
#define MEASUREMENT_DIM 4
#define ACTION_DIM 1

class KheperaRobotState{
public:
  KheperaRobotState(double* state) {
    m_state = state;
  }
  
  void set_reward_location(double reward_location) {m_state[REWARD_DIM] = reward_location;};
  void set_x(double x) {m_state[X_DIM] = x;};
  void set_y(double y) {m_state[Y_DIM] = y;};
  void set_theta(double theta) {m_state[THETA_DIM] = theta;};
  
  /** indicator variable: 1 = robot is at a reward location, 2 otherwise */
  double get_reward_location() {return m_state[REWARD_DIM];};
  double get_x() {return m_state[X_DIM];};
  double get_y() {return m_state[Y_DIM];};
  double get_theta() {return m_state[THETA_DIM];};
  
  enum {REWARD_DIM=0, X_DIM, Y_DIM, THETA_DIM};
  
private:
  double* m_state;
};

/** A plant for demonstration purpose.
  *
  * @ingroup PLANT
  * @ingroup SIMULATION
  **/
class KheperaMazePlant : public Plant {
 public:
  
  KheperaMazePlant();

  /** Computes the next state of the plant, given a current state and an action (state transition).  
   * \param current_plant_state current state of plant.
   * \param current_action executed action in current state.
   * \param next_plant_state resulting state after executing action.
   * \return true to continue, false, if break of episode is requested */
  virtual bool get_next_plant_state(const double *current_plant_state, const double *current_action, double *next_plant_state);

  /** Computes a measurement of the plant_state. 
   * \param plant_state     current state
   * \param observed_state  write observation of current state in this array
   * \return true for success */
  virtual bool get_measurement(const double *plant_state, double *measurement);

  /** Checks, if plant agrees on initial state. If not, the plant module can override the initial state
   *  or reject the initial state by returning false. 
   * \param initial_state initial state
   * \param target_state target state 
   * \returns true, if plant agrees on initial state or overrides the initial state */
  virtual bool check_initial_state(double *initial_plant_state);

  /** Initialize plant.
   * Implement one of the following virtual bool init(...) functions for your plant.
   * For convenience you can use the function that meets your requirements most.
   * Other params are set by default as defined below.
   * \param plant_state_dim plant returns dimension of state vector for plant state.
   * \param measurenebt_dim plant returns dimension of observation vector for controller.
   * \param action_dim plant returns dimension of action space. 
   * \param delta_t plant returns duration of one control cycle in seconds.
   * \param fname File, which contains configuration of plant module
   * \return true, for success. */
  virtual bool init(int &plant_state_dim, int &measurement_dim, int &action_dim, 
		    double &delta_t, const char *fname=0, const char *chapter=0);

  /** Notifies that an episode has been started. */
  virtual void notify_episode_starts();
  
  /** Notifies that an episode has been stopped. */
  virtual void notify_episode_stops();

  /** virtual destructor is necessary since methods declared virtual */
  virtual ~KheperaMazePlant();

  /** Notifies that a command via pipe has arrived. */
  //  virtual void notfiy_command_string(char* buf){return;};
  virtual void notify_command_string(const char* buf);

 protected:
  int verbosity; /** verbosity of demo plant */
  virtual bool read_options(const char *fname, const char *chapter);

private:
  KheperaWorld* m_khepera_world;
  
  /** The size of the individual boxes in meters. */
  double maze_box_size;
};

#endif // _KHEPERA_MAZE_PLANT_H_
