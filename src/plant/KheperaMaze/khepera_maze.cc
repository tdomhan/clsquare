#include "khepera_maze.h"
#include "khepera_world.h"

#include <iostream>

//#include <gl/glut.h>


KheperaMaze::KheperaMaze(b2World* world, const char* maze_definition_file) :
m_world(world),
m_maze_data(maze_definition_file),
m_box_size(ZOOM*0.2),
m_robot_in_goal(false) {
  
  double maze_width = m_box_size * m_maze_data.get_extend_X();
  double maze_height = m_box_size * m_maze_data.get_extend_X();
  
  //a static body
  b2BodyDef wallBodyDef;
  wallBodyDef.type = b2_staticBody;
  wallBodyDef.position.Set(0, 0);
  m_body = m_world->CreateBody(&wallBodyDef);
  
  b2EdgeShape edgeShape;
  
  b2FixtureDef wallFixtureDef;
  wallFixtureDef.shape = &edgeShape;
  
  //the units are meters
  b2Vec2 bl( 0, 0);
  b2Vec2 br( maze_width, 0);
  b2Vec2 tl( 0, maze_height);
  b2Vec2 tr( maze_width, maze_height);
  edgeShape.Set( bl, br ); //ground
  m_body->CreateFixture(&wallFixtureDef);
  edgeShape.Set( tl, tr);//ceiling
  m_body->CreateFixture(&wallFixtureDef);
  edgeShape.Set( bl, tl );//left wall
  m_body->CreateFixture(&wallFixtureDef);
  edgeShape.Set( br, tr );//right wall
  m_body->CreateFixture(&wallFixtureDef);
  
  
  b2BodyDef boxBodyDef;
  b2PolygonShape polygonShape;
  b2FixtureDef boxFictureDef;
  boxFictureDef.shape = &polygonShape;
  boxBodyDef.type = b2_staticBody;
  polygonShape.SetAsBox(0.5*m_box_size, 0.5*m_box_size);
  
  for (int x=0; x<m_maze_data.get_extend_X(); x++) {
    for (int y=0; y<m_maze_data.get_extend_Y(); y++) {
      if(m_maze_data.get_cell(x, y)->get_status() == OBSTACLE) {
        //in box2D Y points upwards, so we have to invert the Y coordinate
        
        boxBodyDef.position.Set(x*m_box_size+0.5*m_box_size, y*m_box_size+0.5*m_box_size);
        m_world->CreateBody(&boxBodyDef)->CreateFixture(&boxFictureDef);
        
      } if(m_maze_data.get_cell(x, y)->get_status() == GOAL) {
        m_goal_cells.push_back(m_maze_data.get_cell(x, y));
        
        std::cout << "goal coordinate:" <<  x << ", " << y << std::endl;
      }
    }
  }
}


KheperaMaze::~KheperaMaze() {
  m_world->DestroyBody(m_body);
}

void KheperaMaze::step() {
  //Check all goal locations and see whether the robot is in them.
  
  m_robot_in_goal = false;
  for (std::vector<Cell*>::iterator it = m_goal_cells.begin(); it != m_goal_cells.end(); it++) {
    Cell* cell = *it;
    int x = cell->get_X();
    int y = cell->get_Y();
    //register a callback for the goal area:
    b2AABB aabb;
    b2Vec2 lower(x*m_box_size, y*m_box_size);
    b2Vec2 upper(x*m_box_size+1.0*m_box_size, y*m_box_size+1.0*m_box_size);
    aabb.lowerBound = lower;
    aabb.upperBound = upper;
    
//    glColor3f(1,1,1);//white
//    glBegin(GL_LINE_LOOP);
//    glVertex2f(lower.x, lower.y);
//    glVertex2f(upper.x, lower.y);
//    glVertex2f(upper.x, upper.y);
//    glVertex2f(lower.x, upper.y);
//    glEnd();
    
    m_world->QueryAABB(this, aabb);
  }
}

bool KheperaMaze::ReportFixture(b2Fixture* fixture) {
  void* user_data = fixture->GetBody()->GetUserData();
  if(user_data != NULL) {
    WORLD_OBJECT_TYPE* type = static_cast<WORLD_OBJECT_TYPE*>(fixture->GetBody()->GetUserData());
    if(*type == ROBOT) {
      std::cout << "ROBOT IN GOAL" << std::endl;
      m_robot_in_goal = true;
    }
  }
}
