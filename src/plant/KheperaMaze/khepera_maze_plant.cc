/*
clsquare - closed loop simulation system
Copyright (c) 2004, Neuroinformatics Group, Prof. Dr. Martin Riedmiller,
University of Osnabrueck
Copyright (c) 2011, Machine Learning Lab, Prof. Dr. Martin Riedmiller,
University of Freiburg

khepera_maze.cc: a 2D maze for the Khepera robot.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the <ORGANIZATION> nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  */

#include <math.h>
#include <stdio.h>
#include <iostream>
#include <string.h>


#include <khepera_world.h>

#include "valueparser.h"
#include "khepera_maze_plant.h"

   
/** The time (for example in seconds) that corresponds to one time tick */
#define DELTA_T_DEFAULT 0.1


KheperaMazePlant::KheperaMazePlant() :
  maze_box_size(0.2)
{
  
}


bool KheperaMazePlant::get_next_plant_state(const double *p_current_plant_state,
				     const double *p_current_action, double *p_next_plant_state){
  
  KheperaRobotState current_plant_state((double*) p_current_plant_state);
  KheperaRobotState next_plant_state(p_next_plant_state);
  
  m_khepera_world->get_khepera()->set_state(current_plant_state.get_x(),
                                            current_plant_state.get_y(),
                                            current_plant_state.get_theta());
  
  KheperaRobot::MoveState action = static_cast<KheperaRobot::MoveState>(p_current_action[0]);
  if(action < KheperaRobot::NUMBER_MOVE_STATES) {
    m_khepera_world->get_khepera()->set_move_state(action);
  } else {
    IOUT("Illegal action");
  }
  
  m_khepera_world->simulate(__delta_t);
  
  next_plant_state.set_x(m_khepera_world->get_khepera()->get_x());
  next_plant_state.set_y(m_khepera_world->get_khepera()->get_y());
  next_plant_state.set_theta(m_khepera_world->get_khepera()->get_theta());
  next_plant_state.set_reward_location((m_khepera_world->get_maze()->is_robot_in_goal()) ? 1.0 : 0.0);

  if(m_khepera_world->get_maze()->is_robot_in_goal()) {
    IOUT("GOAL!");
  }
  
  std::cout << "STATE:" << std::endl;
  std::cout << p_next_plant_state[1] << std::endl;
  std::cout << p_next_plant_state[2] << std::endl;
  std::cout << p_next_plant_state[3] << std::endl;

  
  if(verbosity){
    IOUT( "transition: ("<<p_current_plant_state[0]<<", "<<p_current_plant_state[1]<<")"
	  <<", action: "<<p_current_action[0]<<" -> ("
	  <<p_next_plant_state[0]<<", "<<p_next_plant_state[1]<<")");
  }
  return true;
}


bool KheperaMazePlant::get_measurement(const double *plant_state, double *measurement){
  for (int i=0; i < MEASUREMENT_DIM; i++) {
    measurement[i] = plant_state[i];
  }
  if(verbosity){
    //IOUT("get_measurement called");
    //IOUT( "("<<measurement[0]<<", "<<measurement[1]<<")");
  }
  return true;
}

/** default check_initial_state behavior **/
//TODO: check whether there is a collision
bool KheperaMazePlant::check_initial_state(double *initial_plant_state) 
{
  if(verbosity){
    IOUT( "init state: ("<<initial_plant_state[0]<<", "<<initial_plant_state[1]<<")");
  }
  return true;
}



void KheperaMazePlant::notify_episode_starts(){
  if(verbosity){
    IOUT( "started ");
  }
}


void KheperaMazePlant::notify_episode_stops(){
  if(verbosity){
  IOUT( "stopped ");
  }
}

bool KheperaMazePlant::init(int &plant_state_dim, int &measurement_dim, int &action_dim, 
		 double &delta_t, const char *fname, const char *chapter) {
    
  m_khepera_world = new KheperaWorld();

  __plant_state_dim = STATE_DIM;
  __measurement_dim = MEASUREMENT_DIM;
  __action_dim = ACTION_DIM;
  __delta_t           = DELTA_T_DEFAULT;

  verbosity = 1;

  if(read_options(fname,chapter) == false)
    return false;

  // return values:
  plant_state_dim   = __plant_state_dim;
  measurement_dim   = __measurement_dim;
  action_dim        = __action_dim;
  delta_t           = __delta_t;
  if(verbosity){
    IOUT( "Successfully initialized ");
  }
  return true;
}


void KheperaMazePlant::notify_command_string(const char* buf){
  IOUT("received command string: "<<buf);
  if(strncmp(buf,"plant_cmd say_hello",18) == 0){
    IOUT("Hello World");
  }
  // in non-simulated plants this one may be important!
  else if(strncmp(buf,"plant_cmd pause",15) == 0){
    IOUT("Sleeping...");
  }
  else{
    EOUT("sorry don't understand command string:"<<buf);
  }
}



bool KheperaMazePlant::read_options(const char *fname, const char *chapter)
{
  // Optionen Einlesen
  ValueParser vp(fname,chapter==0?"Plant":chapter);

  //  vp.get("delta_t",delta_t);  

  vp.get("verbosity",verbosity);
  
  vp.get("mazeBoxSize", maze_box_size);

  // Show error-message if necessary
  if ( vp.num_of_not_accessed_entries() ) {
    cerr << "\nPlant: not recognized options:";
    vp.show_not_accessed_entries(cerr);
    cerr << endl;
    return false;
  }

  return true;
}

KheperaMazePlant::~KheperaMazePlant() {
  delete m_khepera_world;
}



REGISTER_PLANT(KheperaMazePlant, "Maze for the Khepera robot.")
