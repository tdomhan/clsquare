#ifndef _KHEPERA_WORLD_H_
#define _KHEPERA_WORLD_H_

#include <Box2D/Box2D.h>

#include <khepera_robot.h>
#include <khepera_maze.h>
#include <khepera_consts.h>


/**
 * A Box2D world that similuates the Khepera robot in a Maze
 */
class KheperaWorld
{
public:
  KheperaWorld();
  
  ~KheperaWorld();
  
  /** Take a single step. (of length m_time_step) */
  void step();
  
  /** Simulate a given number of seconds. */
  void simulate(double seconds);
  
  KheperaRobot* get_khepera() {return m_khepera;};
  KheperaMaze* get_maze() {return m_maze;};
  
private:
  b2World* m_world;
  KheperaRobot* m_khepera;
  KheperaMaze* m_maze;
  
  float32 m_time_step;
  int32 m_velocity_iterations;
  int32 m_position_iterations;
};


#endif
