#ifndef _KHEPERA_ROBOT_H_
#define _KHEPERA_ROBOT_H_

#include <Box2D/Box2D.h>

#include <iostream>
#include <math.h>
#include <vector>

#include <khepera_consts.h>

#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f

class RayCaster {
public:
  
  RayCaster(b2World* world, b2Body* source_body, float m_ray_angle_offset)
  :
  m_world(world),
  m_source_body(source_body),
  m_ray_angle_offset(m_ray_angle_offset),
  m_ray_length(10)
  {
    
  }
  
  /**
   * Cast a ray.
   *
   * returns the distance to the closest object or m_ray_length, when nothing is hit.
   */
  double cast();
  
/*  void drawOpenGL(b2Vec2& intersectionPoint) {
    b2Vec2 source_position = m_source_body->GetPosition();
    //draw a line
    glColor3f(1,1,1); //white
    glBegin(GL_LINES);
    glVertex2f( source_position.x, source_position.y );
    glVertex2f( intersectionPoint.x, intersectionPoint.y );
    glEnd();
    
    //draw a point at the intersection point
    glPointSize(5);
    glBegin(GL_POINTS);
    glVertex2f( intersectionPoint.x, intersectionPoint.y );
    glEnd();
  }
 */
  
private:
  b2World* m_world;
  b2Body* m_source_body;
  float m_ray_angle_offset;
  float m_ray_length;
};


class KheperaRobot {
public:
  enum MoveState {
    MS_STOP=0,
    MS_FORWARD,
    MS_TURN_LEFT,
    MS_TURN_RIGHT,
    NUMBER_MOVE_STATES
  };
  
  
  KheperaRobot(b2World* world);
  
  void step();
  
  void set_move_state(MoveState move_state);
  
  void set_state(double x, double y, double angle);
  
  double get_x();
  
  double get_y();
  
  double get_theta();
  
  ~KheperaRobot();
  
private:
  b2World* m_world;
  b2Body* m_body;
  
  std::vector<RayCaster*> rays;
  
  MoveState m_move_state;
  
  WORLD_OBJECT_TYPE m_type;
};



#endif
