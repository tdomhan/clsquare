#note: depends on the class MazeData from the Maze plant
#for parsing the maze definition file.
INCLUDE_DIRECTORIES(${CURRENT_LIST_DIR})

FIND_PACKAGE(Box2D)
IF (Box2D_FOUND)
    #for some reason CHECK_EXTERNAL expects FOUND_Box2D instead of Box2D_FOUND
   SET(FOUND_Box2D TRUE)

   LIST (APPEND CLSQUARE_INCLUDE_DIRS ${Box2D_INCLUDE_DIRS})
   INCLUDE_DIRECTORIES(${Box2D_INCLUDE_DIRS})
   LIST (APPEND CLSQUARE_LIB_EXTERNAL Box2D)
ENDIF()

CHECK_EXTERNAL(KheperaMazePlant Box2D)

if(NOT MISSING_DEPENDENCY)
    LIST(APPEND plant_srcs 
        ${CURRENT_LIST_DIR}/khepera_maze_plant.cc
        ${CURRENT_LIST_DIR}/khepera_maze.cc
        ${CURRENT_LIST_DIR}/khepera_robot.cc
        ${CURRENT_LIST_DIR}/khepera_world.cc
        ${CURRENT_LIST_DIR}/khepera_mazegraphic.cc
    )
endif()
