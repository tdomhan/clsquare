#include "khepera_world.h"

#include <iostream>



KheperaWorld::KheperaWorld() :
  m_time_step(1.0f/30.0f),
  m_velocity_iterations(6),
  m_position_iterations(2)
{
  b2Vec2 gravity(0.0f, -10.0f);
  m_world = new b2World(gravity);
  
  m_khepera = new KheperaRobot(m_world);
  m_maze = new KheperaMaze(m_world, "maze.def");
}


KheperaWorld::~KheperaWorld() {
  delete m_world;
}

void KheperaWorld::step() {
  m_khepera->step();
  m_maze->step();
  
  //std::cout << "Khepera world step" << std::endl;
  
  //Let the physics engine run for a bit.
  m_world->Step(m_time_step, m_velocity_iterations, m_position_iterations);
}

void KheperaWorld::simulate(double seconds) {
  int steps = (int) (seconds / m_time_step);
  std::cout << steps << std::endl;
  for (int i = 0; i < steps; i++) {
    step();
  }
}
