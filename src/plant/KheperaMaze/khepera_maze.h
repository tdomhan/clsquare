#ifndef _KHEPERA_MAZE_H_
#define _KHEPERA_MAZE_H_

#include <string>
#include <vector>

#include <Box2D/Box2D.h>
#include <mazedata.h>

#include <khepera_consts.h>


/**
 * A maze is built up by boxes that can be placed on a grid
 * as well as the walls surrounding it.
 */
class KheperaMaze : public b2QueryCallback {
public:
  KheperaMaze(b2World* world, const char* maze_definition_file);
  
  ~KheperaMaze();
  
  void step();
  
  bool is_robot_in_goal() {return m_robot_in_goal;};
  
  virtual bool ReportFixture(b2Fixture* fixture);

private:
  b2World* m_world;
  b2Body* m_body;
  MazeData m_maze_data;
  std::vector<Cell*> m_goal_cells;
  double m_box_size;
  bool m_robot_in_goal;
};

#endif
